import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
      let keysPressed = {};
      let firstFighterHealth = firstFighter.health;
      let secondFighterHealth = secondFighter.health;

      let time1 = new Date();
      let time2 = Date;

      const [firstFighterKey1, firstFighterKey2, firstFighterKey3] = controls.PlayerOneCriticalHitCombination;
      const [secondFighterKey1, secondFighterKey2, secondFighterKey3] = controls.PlayerTwoCriticalHitCombination;

      function resolveDamage(health, dmg, attacker) {
        health = health - dmg;
        if (health <= 0) {
          resolve(attacker);
        }
        return health;
      }

      let damage;
      window.addEventListener('keyup', (event) => {
        delete keysPressed[event.code];
      });

      window.addEventListener('keydown', function(event) {
        keysPressed[event.code] = true;

        let now = new Date();
        if (keysPressed[firstFighterKey1] && keysPressed[firstFighterKey2] && keysPressed[firstFighterKey3]) {
          //can't hit yet
          if (time1.valueOf() > now.valueOf()) {
            return;
          }

          time1 = now;
          time1.setSeconds(time1.getSeconds() + 10);

          secondFighterHealth = resolveDamage(secondFighterHealth, firstFighter.attack * 2, firstFighter);
          updateHealthBar('right', (secondFighterHealth / secondFighter.health));

        }
        if (keysPressed[secondFighterKey1] && keysPressed[secondFighterKey2] && keysPressed[secondFighterKey3]) {
          //can't hit yet
          if (time2.valueOf() > now.valueOf()) {
            return;
          }

          time2 = now;
          time2.setSeconds(time2.getSeconds() + 10);

          firstFighterHealth = resolveDamage(firstFighterHealth, secondFighter.attack * 2, secondFighter);
          updateHealthBar('left', (firstFighterHealth / firstFighter.health));
        }

        if (keysPressed[controls.PlayerOneAttack]) {
          if (keysPressed[controls.PlayerOneBlock]) {
            return;
          }
          damage = getDamage(firstFighter, keysPressed[controls.PlayerTwoBlock] ? secondFighter : {});
          secondFighterHealth = resolveDamage(secondFighterHealth, damage, firstFighter);

          updateHealthBar('right', (secondFighterHealth / secondFighter.health));

        }

        if (keysPressed[controls.PlayerTwoAttack]) {
          if (keysPressed[controls.PlayerTwoBlock]) {
            return;
          }
          damage = getDamage(secondFighter, keysPressed[controls.PlayerOneBlock] ? firstFighter : {});
          firstFighterHealth = resolveDamage(firstFighterHealth, damage, secondFighter);
          updateHealthBar('left', (firstFighterHealth / firstFighter.health));

        }
      });
    }
  );
}

async function updateHealthBar(position, percentage) {
  if (percentage == 1) {
    return;
  }
  let healthBar = document.getElementById(`${position}-fighter-indicator`);
  healthBar.style.width = percentage * 100 + '%';
  healthBar.style.backgroundColor = 'hsl(' + 52 * percentage + ', 78.5%, 63.5%)';
}

export function getDamage(attacker, defender) {
  // getHitPower - getBlockPower
  let attack = getHitPower(attacker);

  if (!Object.keys(defender).length) {
    return attack;
  }

  let block = getBlockPower(defender);
  let damage = attack - block;
  // console.log('damage', damage, 'attack', attack, 'block', block);

  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
