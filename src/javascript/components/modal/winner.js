import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  let bodyElem = createElement({ tagName: 'div', className:'modal-body-info'});
  bodyElem.appendChild(createFighterImage(fighter));

  let someText = createElement({ tagName: 'p' });
  someText.innerText = 'To start a new game - refresh this page or hit Refresh';

  bodyElem.appendChild(someText);

  const refreshButton = createElement({ tagName: 'div', className: 'close-btn' });

  refreshButton.innerText = 'Refresh';

  refreshButton.addEventListener('click', location.reload.bind(location));
  bodyElem.append(refreshButton);

  showModal({
    title: `${fighter.name} won!`,
    bodyElement: bodyElem

  });
}
