import { createElement } from '../helpers/domHelper';
import { controls } from '../../constants/controls';

export function createFighterPreview(fighter, position) {
  const fighterPosition = position === 'right';
  const positionClassName = fighterPosition ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });


  if (!fighter) {
    return fighterElement;
  }

  fighterElement.appendChild(createFighterImage(fighter));

  let info = createElement({ tagName: 'div', className: 'fighter-preview___info' });

  let infoHeader = createElement({ tagName: 'h3' });
  infoHeader.innerText = 'Player info';
  info.appendChild(infoHeader);

  let infoTable = createTableFromRows({
    'Name': fighter.name,
    'Health': fighter.health,
    'Attack': fighter.attack,
    'Defense': fighter.defense
  });

  info.appendChild(infoTable);


  let keysHeader = createElement({ tagName: 'h3' });
  keysHeader.innerText = 'Keys';
  info.appendChild(keysHeader);

  let keysTable = createTableFromRows({
    'Attack': fighterPosition ? controls.PlayerTwoAttack : controls.PlayerOneAttack,
    'Block': fighterPosition ? controls.PlayerTwoBlock : controls.PlayerOneBlock,
    'Critical Hit': fighterPosition ?
      controls.PlayerTwoCriticalHitCombination.join('+') :
      controls.PlayerOneCriticalHitCombination.join('+')
  });
  info.appendChild(keysTable);


  fighterElement.appendChild(info);

  return fighterElement;
}

function createTableFromRows(tableMap) {
  let table = createElement({ tagName: 'table', className: 'fighter-preview___info-table' });

  Object.keys(tableMap).forEach((key) =>
    table.appendChild(createTableRow(key, tableMap[key])));
  return table;
}

function createTableRow(key, name) {
  let tr = createElement({ tagName: 'tr' });

  tr.appendChild(createTableTd(key));
  tr.appendChild(createTableTd(name));

  return tr;
}

function createTableTd(name) {
  let nameEl = createElement({ tagName: 'td' });
  nameEl.innerText = name;
  return nameEl;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
